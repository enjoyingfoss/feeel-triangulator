const IMAGE_TYPE = "IMAGE"

export function getImagePaint(imageNode: RectangleNode): ImagePaint {
  let fills = imageNode.fills

  if (fills == figma.mixed) {
    throw Error("Must be an image fill (this is type 'mixed')")
  }

  for (let fill of fills) {
    if (fill.type == IMAGE_TYPE) {
      return fill as ImagePaint
    }
  }

  throw Error("Must have an image as a fill!")
}
