export default function getTriangles(
  vertexArrays: Array<Array<number>>,
  triangles: Uint32Array
): VectorNode[] {
  let triangleNodes: VectorNode[] = []
  for (let i = 0; i < triangles.length; i += 3) {
    let a = vertexArrays[triangles[i]],
      b = vertexArrays[triangles[i + 1]],
      c = vertexArrays[triangles[i + 2]]

    console.log(`M ${a[0]} ${a[1]} L ${b[0]} ${b[1]} L ${c[0]} ${c[1]} Z`)

    let vectorNode = figma.createVector()
    vectorNode.vectorPaths = [
      {
        windingRule: "EVENODD",
        data: `M ${a[0]} ${a[1]} L ${b[0]} ${b[1]} L ${c[0]} ${c[1]} Z`,
      },
    ]
    triangleNodes.push(vectorNode)
  }
  return triangleNodes
}
