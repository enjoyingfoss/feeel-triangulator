import Delaunator from "delaunator"
import jsfeat from "jsfeat"
import { getImagePaint } from "./getImagePaint"
import getVectorNetwork from "./getVectorNetwork"

const RECTANGLE_NODE = "RECTANGLE"

let selection = figma.currentPage.selection

if (selection[0].type != RECTANGLE_NODE)
  throw Error("You must select a single image")

const imagePaint = getImagePaint(selection[0] as RectangleNode)
const image = figma.getImageByHash(imagePaint.imageHash)

image.getBytesAsync().then(function (bytes) {
  figma.showUI(__html__, { visible: false })
  figma.ui.postMessage(bytes)
  new Promise((resolve, reject) => {
    figma.ui.onmessage = (value) => resolve(value)
  }).then(function (imageMatrix: any) { //todo what is the type of imageMatrix?
    // todo get RGB image pixels from hidden layer

    // var my_matrix = new jsfeat.matrix_t(
    //   columns,
    //   rows,
    //   data_type,
    //   (data_buffer = undefined)
    // )
  })
})

// const FRAME_NODE = "FRAME"
// const VECTOR_NODE = "VECTOR" //todo is there some file type somewhere to do this for me?

// let vertexArrays: Array<Array<number>> = []

// let selection = figma.currentPage.selection

// // Get nodes
// for (let node of selection) {
//   if (node.type == VECTOR_NODE) {
//     const center = [node.width / 2 + node.x, node.height / 2 + node.y]
//     vertexArrays.push(center)
//   }
// }

// // Get parent frame
// let tempParent: any = selection[0]
// do {
//   if (tempParent == null) {
//     throw Error("Selection must be inside a frame") //todo draw frame if needed
//   }
//   tempParent = selection[0].parent
// } while (tempParent.type != FRAME_NODE)

// let parentFrame = tempParent as FrameNode

// // Triangulate
// const delaunator = Delaunator.from(vertexArrays)

// // Draw triangulation
// parentFrame.appendChild(getVectorNetwork(vertexArrays, delaunator.triangles))

figma.closePlugin()
