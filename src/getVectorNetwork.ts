export default function getVectorNetwork(
  vertexArrays: Array<Array<number>>,
  triangles: Uint32Array
): VectorNode {
  let vertices = vertexArrays.map(function (
    vertices: Array<number>
  ): VectorVertex {
    return { x: vertices[0], y: vertices[1] }
  })

  let segments: VectorSegment[] = []
  let loops: Array<Array<number>> = []
  for (let i = 0; i < triangles.length; i += 3) {
    segments.push({
      start: triangles[i],
      end: triangles[i + 1],
    })
    segments.push({
      start: triangles[i + 1],
      end: triangles[i + 2],
    })
    segments.push({
      start: triangles[i + 2],
      end: triangles[i],
    })

    loops.push([segments.length - 3, segments.length - 2, segments.length - 1])
  }

  const vectorNode = figma.createVector()

  vectorNode.vectorNetwork = {
    vertices: vertices,
    segments: segments,
    regions: [{ windingRule: "NONZERO", loops: loops }],
  }

  return vectorNode
}
